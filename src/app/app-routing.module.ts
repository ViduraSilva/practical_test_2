import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
  },
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full",
  },
  {
    path: "login",
    loadChildren: () =>
      import("./login/login.module").then((m) => m.LoginPageModule),
  },

  {
    path: "main-ui",
    loadChildren: () =>
      import("./main-ui/main-ui.module").then((m) => m.MainUiPageModule),
  },
  {
    path: "create-employee",
    loadChildren: () =>
      import("./create-employee/create-employee.module").then(
        (m) => m.CreateEmployeePageModule
      ),
  },
  {
    path: "employee-details",
    loadChildren: () =>
      import("./employee-details/employee-details.module").then(
        (m) => m.EmployeeDetailsPageModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
